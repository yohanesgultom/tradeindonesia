<h3>Requester</h3>
<ul>
    <li>Full name: {{ $request->fullname }}</li>
    <li>Company: {{ $request->company }}</li>
    <li>Position: {{ $request->position }}</li>
    <li>Country: {{ $request->country }}</li>
    <li>E-mail: {{ $request->email }}</li>
    <li>Phone: {{ $request->phone }}</li>
</ul>

<h3>Details</h3>
<ul>
    <li>Keywords: {{ $request->keywords }}</li>
    <li>Category: {{ $request->category }}</li>
    <li>Quantity: {{ number_format($request->qty) }} {{ \App\Models\Request::UNITS[$request->unit] }}</li>
    <li>Shipping: {{ $request->shipping }} to {{ $request->destination }}</li>
    <li>Preffered unit price: {{ $request->currency }} {{ number_format($request->unit_price, 2) }}</li>
    <li>Payement methods: {{ $request->payment }}</li>
    <li>Details: <p>{{ $request->details }}</p></li>
    <li>Request create time: {{ $request->created_at }}</li>
    <li>Request update time: {{ $request->updated_at }}</li>
</ul>
