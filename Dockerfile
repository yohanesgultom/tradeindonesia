FROM php:7.1.33-cli
RUN apt-get update && apt-get install -y \
    build-essential \
    libpng-dev \
    libjpeg62-turbo-dev \
    libfreetype6-dev \
    libonig-dev \
    libzip-dev \
    libxml2-dev \
    libpcre3-dev \
    locales \
    jpegoptim optipng pngquant gifsicle \
    zip \
    unzip \
    git \
    curl

# Laravel
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
RUN apt-get clean && rm -rf /var/lib/apt/lists/*
RUN docker-php-ext-install pdo_mysql mbstring exif pcntl bcmath gd zip
