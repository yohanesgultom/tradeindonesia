<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Public routes (do not require authentication)
 */
Route::get('/', 'PageController@index')->name('page.index');
Route::get('/product/{slug}', 'PageController@product')->name('page.product');
Route::post('/rfq-details', 'PageController@rfqDetails')->name('post.rfq-details');

/**
 * Auth routes (public)
 */
Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');

/**
 * Admin routes (require authentication)
 */
Route::get('/admin', 'HomeController@index');
Route::get('/admin/requests/download-attachment', 'RequestController@downloadAttachment')->name('request.downloadAttachment');
Route::resource('requests', 'RequestController');
Route::resource('users', 'UserController');
Route::resource('sliders', 'SliderController');
Route::resource('products', 'ProductController');
Route::resource('testimonials', 'TestimonialController');
Route::resource('whyus', 'WhyUsController');
