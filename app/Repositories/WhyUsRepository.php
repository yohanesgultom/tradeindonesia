<?php

namespace App\Repositories;

use App\Models\WhyUs;
use InfyOm\Generator\Common\BaseRepository;

class WhyUsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'title',
        'subtitle'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return WhyUs::class;
    }
}
