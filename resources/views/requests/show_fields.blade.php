<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $request->id !!}</p>
</div>

<!-- Fullname Field -->
<div class="form-group">
    {!! Form::label('fullname', 'Full Name:') !!}
    <p>{!! $request->fullname !!}</p>
</div>

<!-- Position Field -->
<div class="form-group">
    {!! Form::label('position', 'Position:') !!}
    <p>{!! $request->position !!}</p>
</div>

<!-- Company Field -->
<div class="form-group">
    {!! Form::label('company', 'Company:') !!}
    <p>{!! $request->company !!}</p>
</div>

<!-- Country Field -->
<div class="form-group">
    {!! Form::label('country', 'Country:') !!}
    <p>{!! $request->country !!}</p>
</div>

<!-- Email Field -->
<div class="form-group">
    {!! Form::label('email', 'E-mail:') !!}
    <p>{!! $request->email !!}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', 'Phone Number:') !!}
    <p>{!! $request->phone !!}</p>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('keywords', 'Keywords:') !!}
    <p>{!! $request->keywords !!}</p>
</div>

<!-- Keywords Field -->
<div class="form-group">
    {!! Form::label('category', 'Category:') !!}
    <p>{!! $request->category !!}</p>
</div>


<!-- Qty Field -->
<div class="form-group">
    {!! Form::label('qty', 'Qty:') !!}
    <p>{!! $request->qty !!}</p>
</div>

<!-- Unit Field -->
<div class="form-group">
    {!! Form::label('unit', 'Unit:') !!}
    <p>{!! \App\Models\Request::UNITS[$request->unit] !!}</p>
</div>

<!-- Details Field -->
<div class="form-group">
    {!! Form::label('details', 'Details:') !!}
    <p>{!! $request->details !!}</p>
</div>

<!-- Shipping Field -->
<div class="form-group">
    {!! Form::label('shipping', 'Shipping:') !!}
    <p>{!! $request->shipping !!}</p>
</div>

<!-- Unit Price Field -->
<div class="form-group">
    {!! Form::label('unit_price', 'Unit Price:') !!}
    <p>{!! $request->unit_price !!}</p>
</div>

<!-- Currency Field -->
<div class="form-group">
    {!! Form::label('currency', 'Currency:') !!}
    <p>{!! $request->currency !!}</p>
</div>

<!-- Destination Field -->
<div class="form-group">
    {!! Form::label('destination', 'Destination:') !!}
    <p>{!! $request->destination !!}</p>
</div>

<!-- Payment Field -->
<div class="form-group">
    {!! Form::label('payment', 'Payment:') !!}
    <p>{!! $request->payment !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $request->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $request->updated_at !!}</p>
</div>

<!-- Attachments -->
<div class="form-group">
    {!! Form::label('attachment', 'Attachments:') !!}
    @foreach ($request->attachments as $attachment)
    <div>
        <a href="{{ route('request.downloadAttachment', ['path' => $attachment->path]) }}">
        {{ $attachment->filename }}
        </a>
    </div>
    @endforeach
</div>
