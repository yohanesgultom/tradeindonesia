<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateWhyusesTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whyus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 20)->notNullable();
            $table->string('subtitle', 100)->notNullable();
            $table->string('icon_class', 30);
            $table->string('icon_color', 7);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('whyus');
    }
}
