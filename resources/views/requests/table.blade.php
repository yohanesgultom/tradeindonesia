<table class="table table-responsive" id="requests-table">
    <thead>
        <th>Full Name</th>
        <th>Company</th>
        <th>Country</th>
        <th>E-mail</th>
        <th>Phone Number</th>
        <th>Category</th>
        <th>Keywords</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($requests as $request)
        <tr>
            <td>{!! $request->fullname !!}</td>
            <td>{!! $request->company !!}</td>
            <td>{!! $request->country !!}</td>
            <td>{!! $request->email !!}</td>
            <td>{!! $request->phone !!}</td>
            <td>{!! $request->category !!}</td>
            <td>{!! $request->keywords !!}</td>
            <td>
                {!! Form::open(['route' => ['requests.destroy', $request->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('requests.show', [$request->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    <a href="{!! route('requests.edit', [$request->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
