<table class="table table-responsive" id="testimonials-table">
    <thead>
        <th>Source</th>
        <th>Rating</th>
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($testimonials as $testimonial)
        <tr>
            <td>{!! $testimonial->source !!}</td>
            <td>{!! $testimonial->rating !!}</td>
            <td>
                {!! Form::open(['route' => ['testimonials.destroy', $testimonial->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('testimonials.edit', [$testimonial->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
