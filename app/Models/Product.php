<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Product
 * @package App\Models
 * @version August 18, 2017, 8:03 am UTC
 */
class Product extends Model
{
    use SoftDeletes;

    public $table = 'products';

    public const storageDir = [
        'profile' => 'product-profile',
        'background' => 'product-background',
    ];

    protected $dates = ['deleted_at'];

    public $fillable = [
        'name',
        'content',
        'image_background',
        'image_profile',
        'description',
        'image_profile_file',
        'image_background_file',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'content' => 'string',
        'image_background' => 'string',
        'image_profile' => 'string',
        'description' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'image_profile_file' => 'sometimes|required|max:250|mimetypes:image/png,image/jpeg,image/jpg',
        'image_background_file' => 'sometimes|required|max:650|mimetypes:image/png,image/jpeg,image/jpg',
    ];

    public function setImageProfileFileAttribute($uploadedFile)
    {
        $this->setImage($uploadedFile, 'profile', 'image_profile');
    }

    public function setImageBackgroundFileAttribute($uploadedFile)
    {
        $this->setImage($uploadedFile, 'background', 'image_background');
    }

    private function setImage($uploadedFile, $type, $column)
    {
        if (!empty($uploadedFile) && array_key_exists($type, self::storageDir)) {
          $this->attributes[$column] = $uploadedFile->store(self::storageDir[$type], 'public');
        }
    }

}
