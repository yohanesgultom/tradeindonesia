<!-- Title Field -->
<div class="form-group col-sm-6">
    {!! Form::label('title', 'Title:') !!}
    {!! Form::text('title', null, ['class' => 'form-control', 'required' => 'true']) !!}
</div>

<!-- Subtitle Field -->
<div class="form-group col-sm-6">
    {!! Form::label('subtitle', 'Subtitle:') !!}
    {!! Form::text('subtitle', null, ['class' => 'form-control', 'required' => 'true']) !!}
</div>

<!-- Icon Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon_class', 'Icon Path:') !!}
    {!! Form::text('icon_class', null, ['class' => 'form-control', 'required' => 'true']) !!}
</div>

{{--
<!-- Icon Color Field -->
<div class="form-group col-sm-6">
    {!! Form::label('icon_color', 'Icon Color:') !!}
    <input type="color" name="icon_color" class="form-control" value="{{ $whyus->icon_color or ''  }}">
</div>
--}}

<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('whyus.index') !!}" class="btn btn-default">Cancel</a>
</div>
