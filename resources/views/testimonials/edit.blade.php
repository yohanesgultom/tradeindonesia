@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Testimonial
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($testimonial, ['route' => ['testimonials.update', $testimonial->id], 'method' => 'patch']) !!}

                        @include('testimonials.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection