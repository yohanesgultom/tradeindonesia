<?php

use Illuminate\Database\Seeder;

class WhyUsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('whyus')->insert(
            [
              [
                'title' => '+12K Clients',
                'subtitle' => 'Seamlessly empower fully researched growth strategies.',
                'icon_class' => 'pixicon-profile-male',
                'icon_color' => '#2dc0e8',
                'created_at' => new \DateTime(),
              ],
              [
                'title' => '+60K Sales',
                'subtitle' => 'Seamlessly empower fully researched growth strategies.',
                'icon_class' => 'pixicon-piechart',
                'icon_color' => '#f77825',
                'created_at' => new \DateTime(),
              ],
              [
                'title' => '+3M Revenue',
                'subtitle' => 'Seamlessly empower fully researched growth strategies.',
                'icon_class' => 'pixicon-wallet',
                'icon_color' => '#82b541',
                'created_at' => new \DateTime(),
              ],
              [
                'title' => '+45 Projects',
                'subtitle' => 'Seamlessly empower fully researched growth strategies.',
                'icon_class' => 'pixicon-lightbulb',
                'icon_color' => '#fbb034',
                'created_at' => new \DateTime(),
              ],
            ]
        );
    }
}
