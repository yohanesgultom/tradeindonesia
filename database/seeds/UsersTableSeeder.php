<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'admin',
          'email' => env('MAIL_USERNAME'),
          'password' => bcrypt(env('MAIL_PASSWORD')),
          'created_at' => new \DateTime()
      ]);
    }
}
