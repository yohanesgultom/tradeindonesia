<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use App\Repositories\RequestRepository;
use App\Models\Slider;
use App\Models\Product;
use App\Models\Testimonial;
use App\Models\WhyUs;
use App\User;

class PageController extends Controller
{

    /** @var  RequestRepository */
    private $requestRepository;

    public function __construct(RequestRepository $requestRepo)
    {
        $this->requestRepository = $requestRepo;
    }

    public function index()
    {
        return view('welcome', [
            'sliders' => Slider::orderBy('id', 'asc')->get(),
            'products' => Product::all(),
            'whyusList' => WhyUs::all(),
            'testimonials' => Testimonial::orderBy('rating', 'desc')->take(5)->get()
        ]);
    }

    public function product(Request $request, $slug)
    {
        return view('product', [
            'products' => Product::all(),
            'product' => Product::where('slug', $slug)->first()
        ]);
    }

    public function rfqDetails(Request $request)
    {
        $input = $request->all();
        $requestItem = $this->requestRepository->create($input);
        $attachments = [];
        if ($request->hasFile('attachmentFiles')) {
            foreach ($request->attachmentFiles as $file) {
                $filename = $requestItem->id .'_'. $file->getClientOriginalName();
                $filemime = $file->getClientMimeType();
                array_push($attachments, [
                    'request_id' => $requestItem->id,
                    'path' => $file->storeAs('request-attachments', $filename),
                    'filename' => $filename,
                    'mime' => $filemime,
                    'created_at' => new \DateTime()
                ]);
            }
            \App\Models\RequestAttachment::insert($attachments);
        }

        foreach (User::all() as $user) {
            Mail::to($user->email)->send(new \App\Mail\Request($requestItem, $attachments));
            Log::info('Request e-mail sucessfully sent to: '.$user->email);
        }

        return redirect(route('page.index'));
    }

}
