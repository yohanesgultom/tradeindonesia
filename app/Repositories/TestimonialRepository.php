<?php

namespace App\Repositories;

use App\Models\Testimonial;
use InfyOm\Generator\Common\BaseRepository;

class TestimonialRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'source',
        'rating'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Testimonial::class;
    }
}
