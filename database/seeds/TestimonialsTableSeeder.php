<?php

use Illuminate\Database\Seeder;

class TestimonialsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $testimonials = [
            [
                'source' => 'Unbounce.com',
                'content' => 'Great service with fast and reliable support The design work and detail put into themes are great.',
                'rating' => 5
            ],
            [
                'source' => 'Localhost',
                'content' => 'Superb.',
                'rating' => 4 
            ],
        ];
        DB::table('testimonials')->insert($testimonials);
    }
}
