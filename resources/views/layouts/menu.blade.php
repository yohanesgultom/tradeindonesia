<li class="{{ Request::is('products*') ? 'active' : '' }}">
    <a href="{!! route('products.index') !!}"><i class="fa fa-edit"></i><span>Products</span></a>
</li>

<li class="{{ Request::is('requests*') ? 'active' : '' }}">
    <a href="{!! route('requests.index') !!}"><i class="fa fa-edit"></i><span>Requests</span></a>
</li>

<li class="{{ Request::is('sliders*') ? 'active' : '' }}">
    <a href="{!! route('sliders.index') !!}"><i class="fa fa-edit"></i><span>Sliders</span></a>
</li>

<li class="{{ Request::is('testimonials*') ? 'active' : '' }}">
    <a href="{!! route('testimonials.index') !!}"><i class="fa fa-edit"></i><span>Testimonials</span></a>
</li>

<li class="{{ Request::is('users*') ? 'active' : '' }}">
    <a href="{!! route('users.index') !!}"><i class="fa fa-edit"></i><span>Users</span></a>
</li>
<li class="{{ Request::is('whyus*') ? 'active' : '' }}">
    <a href="{!! route('whyus.index') !!}"><i class="fa fa-edit"></i><span>Why Us</span></a>
</li>
