// jQuery Initialization
jQuery(document).ready(function($){
"use strict";

    // header background
    const style = 'linear-gradient(rgba(0,0,0,0.50),rgba(0,0,0,0.50)), black url({0}) fixed no-repeat center';
    $('#section_intro_3').css('background', style.replace('{0}', $('#section_normal_4_1').attr('data-img')));

});
