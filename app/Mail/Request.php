<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;

class Request extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $request;
    public $files;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(\App\Models\Request $request, $files)
    {
        $this->request = $request;
        $this->files = $files;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this
            ->subject('New RFQ from '.$this->request->fullname)
            ->view('emails.request');
        foreach ($this->files as $file) {
            $path = Storage::getDriver()->getAdapter()->applyPathPrefix($file['path']);
            //  Log::info('Attaching '.$path);
            $mail->attach($path, [
                'as' => $file['filename'],
                'mime'  => $file['mime'],
            ]);
        }
        return $mail;
    }
}
