<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRequestsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname', 100);
            $table->string('company', 100);
            $table->string('position', 100);
            $table->string('country', 100);
            $table->string('email', 100);
            $table->string('phone', 20);
            $table->string('keywords');
            $table->string('category', 100);
            $table->double('qty', 12, 2);
            $table->string('unit', 20);
            $table->text('details');
            $table->string('shipping', 20);
            $table->double('unit_price', 12, 2);
            $table->string('currency', 10);
            $table->string('destination');
            $table->string('payment', 10);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('requests');
    }
}
