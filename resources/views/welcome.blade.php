@extends('layouts.public')

@section('title', env('APP_NAME'))

@section('styles')
    <link rel="stylesheet" href="{{ asset('jquery.bxslider/jquery.bxslider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('jquery.rateyo/jquery.rateyo.min.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('jquery.bxslider/jquery.bxslider.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('jquery.rateyo/jquery.rateyo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('flatpack/js/welcome.js') }}" type="text/javascript"></script>
    <script src="{{ asset('flatpack/js/rfq.js') }}" type="text/javascript"></script>
@endsection

@section('header')
<div class="container">
    @if (! empty($sliders))
    <ul id="bxslider-header">
        @foreach ($sliders as $slider)
        <li data-img="/storage/{{ $slider->path }}">
            <div class="sixteen columns margin_bottom_50 padding_top_60">
                <div class="twelve offset-by-two columns">
                    <div class="center_text big_padding">
                        <p class="big_title bold_text editContent" style="outline: none; cursor: default;">
                            {{ $slider->title }}
                        </p>
                        <p class="big_text normal_gray editContent" style="outline: none; cursor: default;">
                            {!! $slider->description !!}
                        </p>
                        <a href="{{ $slider->btn_url }}" class="pix_button pix_button_line white_border_button bold_text big_text btn_big">
                            <i class="pi pixicon-paper"></i>
                            <span>{{ $slider->btn_text }}</span>
                        </a>
                    </div>
                </div>
            </div>
        </li>
        @endforeach
    </ul>
    @endif
    <div class="center_text">
        <a href="#{{ str_slug($products[0]->name) }}" class="intro_arrow">
            <i class="pi pixicon-arrow-down" style="outline: none; cursor: default;"></i>
        </a>
    </div>
</div>
@endsection

@section('content')
<div class="pixfort_corporate_2" id="banner_1">
    <div class="awesome_style pix_builder_bg" style="background-color: black; outline: none; cursor: default;">
        <div class="container">
            <div class="sixteen columns">
                <br>
                <p class="txt_awesome editContent" style="outline: none; cursor: default;">
                    "Your trusted trade partner on sourcing Indonesia commodity products"
                </p>
                <br>
            </div>
        </div>
        <!-- container -->
    </div>
</div>

@if (!$products->isEmpty())
<!-- Products -->
<h2 id="products-title" class="center_text">Products</h2>
@foreach ($products as $i => $p)
<div id="{{ $p->slug }}" class="pixfort_normal_1">
    <div class="m6_style pix_builder_bg" style="outline: none; cursor: default;">
        <div class="container">
            <div class="sixteen columns">
                @if ($i % 2 != 0)
                <div class="eight columns omega">
                    <img src="/storage/{{ $p->image_profile }}" class="img_style" alt="{{ $p->name }}" style="outline: none; cursor: default;">
                </div>
                @endif
                <div class="eight columns alpha">
                    <div class="gtext_style">
                        <p class="t1_style editContent" style="outline: none; cursor: default;">
                            {{ $p->name }}
                        </p>
                        <p class="t2_style editContent" style="outline: none; cursor: default;">
                            {!! $p->description !!}
                        </p>
                        <p class="t3_style editContent" style="outline: none; cursor: default;">
                            {{ Custom::html_limit($p->content, 500, '') }}
                        </p>
                        {{-- <p class="t3_style editContent" style="outline: none; cursor: default;">
                            {{ Custom::html_limit($p->content, 320) }} <a href="{{ route('page.product', ['slug' => $p->slug]) }}">Details</a>
                        </p> --}}
                    </div>
                </div>
                @if ($i % 2 == 0)
                <div class="eight columns omega">
                    <img src="/storage/{{ $p->image_profile }}" class="img_style" alt="{{ $p->name }}" style="outline: none; cursor: default;">
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endforeach
@endif

@if (!$whyusList->isEmpty())
<!-- why us -->
<div id="why-us">
    <div class="light_gray_bg big_padding pix_builder_bg">
        <h2 class="center_text">Why Us</h2>
        <br><br>        
        <div class="container">
            <div class="sixteen columns">
                @foreach ($whyusList as $whyus)
                <div class="four columns onethird_style alpha">
                    <div class="f1_box center_text">
                        <div class="big_icon" style="color: {{ $whyus->icon_color or 'blue' }}">
                            <span class="pi {{ $whyus->icon_class or 'pixicon-piechart' }}"></span>
                        </div>                    
                        {{--<img src="{{ asset($whyus->icon_class) }}" alt="{{ $whyus->title }}" height="200">--}}
                        <div class="center_text">
                            <span class="normal_title bold_text">
                                <span class="editContent">
                                    <span class="pix_text">{{ $whyus->title or '+60K Sales' }}</span>
                                </span>
                            </span>
                            <br>
                            <span class="editContent normal_text light_gray">
                                <span class="pix_text">{{ $whyus->subtitle or 'Seamlessly empower fully researched growth strategies.'}}</span>
                            </span>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endif

@include('rfq', [
    'background_image' => asset('/flatpack/images/main/bg-form2.jpg'),
    'products' => $products
])

<!-- sponsors -->
<div class="pixfort_university_6" id="section_university_3">
    <div class="logos_sect pix_builder_bg" style="outline: none; cursor: default;">
        <div class="container">
            <div class="row padd_updown">
                <div class="two columns"> <img src="{{ asset('images/logo-kemendag.png') }}" alt="Kementerian Perdagangan RI" style="outline: none; cursor: default;"></div>
                <div class="two columns"> <img src="{{ asset('images/logo-kementan.png') }}" alt="Kementerian Pertanian RI" style="outline: none; cursor: default;"></div>
                <div class="three columns"> <img src="{{ asset('images/logo-kemenperin.png') }}" alt="Kementerian Perindustrian RI" style="outline: none; cursor: default;"></div>
                <div class="two columns"> <img src="{{ asset('images/logo-kesdm.png') }}" alt="Kementerian Energi dan Sumber Daya Mineral RI" style="outline: none; cursor: default;"></div>
                <div class="two columns"> <img src="{{ asset('images/logo-kkp.png') }}" alt="Kementerian Kelautan dan Perikanan RI" style="outline: none; cursor: default;"></div>
                <div class="three columns"> <img src="{{ asset('images/logo-kkukm.png') }}" alt="Kementerian Koperasi dan Usaha Kecil dan Menengah RI" style="outline: none; cursor: default;"></div>
                <div class="two columns"> <img src="{{ asset('images/logo-klhk.png') }}" alt="Kementerian Lingkungan Hidup dan Kehutanan RI" style="outline: none; cursor: default;"></div>
            </div>
        </div>
    </div>
</div>

@if (!$testimonials->isEmpty())
<div class="pixfort_corporate_2" id="section_corporate_8">
    <div class="awesome_style pix_builder_bg" style="outline: none; cursor: default;">
        <div class="container">
            <div class="sixteen columns">
                <h1 class="h_awesome editContent" style="outline: none; cursor: default;"> What our awesome clients say </h1>
                <ul id="bxslider-testimonials">
                    @foreach ($testimonials as $t)
                    <li>
                        <p class="txt_awesome editContent" style="outline: none; cursor: default;">
                            {{ $t->content }}
                        </p>
                        <div class="via_st row">
                            <div class="rateyo offset-by-six three columns" data-rateyo-rating="{{ $t->rating }}"></div>
                            <div class="top_3 editContent pix_text three columns" style="outline: none; cursor: default; text-align: left; margin-left: 0; padding-top: 5px;">
                                via {{ $t->source }}
                            </div>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- container -->
    </div>
</div>
@endif
@endsection
