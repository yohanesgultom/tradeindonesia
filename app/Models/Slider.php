<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Log;

/**
 * Class Slider
 * @package App\Models
 * @version August 12, 2017, 10:54 am UTC
 */
class Slider extends Model
{
    use SoftDeletes;

    public $table = 'sliders';

    public const storageDir = 'sliders';

    protected $dates = ['deleted_at'];

    public $fillable = [
        'title',
        'description',
        'btn_text',
        'btn_url',
        'image'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'filename' => 'string',
        'path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'image' => 'required|max:650|mimetypes:image/png,image/jpeg,image/jpg'
    ];

    public function setImageAttribute($uploadedFile)
    {
        if (!empty($uploadedFile)) {
          $this->attributes['filename'] = $uploadedFile->getClientOriginalName();
          $this->attributes['path'] = $uploadedFile->store(self::storageDir, 'public');
        }
    }

}
