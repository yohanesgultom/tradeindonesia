<table class="table table-responsive" id="whyus-table">
    <thead>
        <th>Title</th>
        <th>Subtitle</th>
        <th>Icon</th>
        {{--<th>Color</th>--}}
        <th colspan="3">Action</th>
    </thead>
    <tbody>
    @foreach($whyusList as $whyus)
        <tr>
            <td>{!! $whyus->title !!}</td>
            <td>{!! $whyus->subtitle !!}</td>
            <td>{!! $whyus->icon_class !!}</td>
            {{--<td><span class="pi {{ $whyus->icon_class }}"></span></td>--}}
            {{--<td style="background-color: {{ $whyus->icon_color }}">{{ $whyus->icon_color }}</td>--}}
            <td>
                {!! Form::open(['route' => ['whyus.destroy', $whyus->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('whyus.edit', [$whyus->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
