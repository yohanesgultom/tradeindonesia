<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateWhyUsRequest;
use App\Http\Requests\UpdateWhyUsRequest;
use App\Repositories\WhyUsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class WhyUsController extends AppBaseController
{
    /** @var  WhyUsRepository */
    private $whyUsRepository;

    public function __construct(WhyUsRepository $whyUsRepo)
    {
        $this->whyUsRepository = $whyUsRepo;
    }

    /**
     * Display a listing of the WhyUs.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->whyUsRepository->pushCriteria(new RequestCriteria($request));
        $whyusList = $this->whyUsRepository->all();

        return view('whyus.index')
            ->with('whyusList', $whyusList);
    }

    /**
     * Show the form for creating a new WhyUs.
     *
     * @return Response
     */
    public function create()
    {
        return view('whyus.create');
    }

    /**
     * Store a newly created WhyUs in storage.
     *
     * @param CreateWhyUsRequest $request
     *
     * @return Response
     */
    public function store(CreateWhyUsRequest $request)
    {
        $input = $request->all();

        $whyus = $this->whyUsRepository->create($input);

        Flash::success('Why Us saved successfully.');

        return redirect(route('whyus.index'));
    }

    /**
     * Display the specified WhyUs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $whyus = $this->whyUsRepository->findWithoutFail($id);

        if (empty($whyus)) {
            Flash::error('Why Us not found');

            return redirect(route('whyus.index'));
        }

        return view('whyus.show')->with('whyus', $whyus);
    }

    /**
     * Show the form for editing the specified WhyUs.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $whyus = $this->whyUsRepository->findWithoutFail($id);

        if (empty($whyus)) {
            Flash::error('Why Us not found');

            return redirect(route('whyus.index'));
        }

        return view('whyus.edit')->with('whyus', $whyus);
    }

    /**
     * Update the specified WhyUs in storage.
     *
     * @param  int              $id
     * @param UpdateWhyUsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateWhyUsRequest $request)
    {
        $whyus = $this->whyUsRepository->findWithoutFail($id);

        if (empty($whyus)) {
            Flash::error('Why Us not found');

            return redirect(route('whyus.index'));
        }

        $whyus = $this->whyUsRepository->update($request->all(), $id);

        Flash::success('Why Us updated successfully.');

        return redirect(route('whyus.index'));
    }

    /**
     * Remove the specified WhyUs from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $whyus = $this->whyUsRepository->findWithoutFail($id);

        if (empty($whyus)) {
            Flash::error('Why Us not found');

            return redirect(route('whyus.index'));
        }

        $this->whyUsRepository->delete($id);

        Flash::success('Why Us deleted successfully.');

        return redirect(route('whyus.index'));
    }
}
