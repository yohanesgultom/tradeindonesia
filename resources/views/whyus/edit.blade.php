@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Why Us
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($whyus, ['route' => ['whyus.update', $whyus->id], 'method' => 'patch']) !!}

                        @include('whyus.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection
