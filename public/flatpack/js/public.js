// jQuery Initialization
jQuery(document).ready(function($){
"use strict";

    // dropdown menu
    $('.dropdown').hover(
        function () {$(this).addClass('open');},
        function () {$(this).removeClass('open');}
    );

    // smooth scrolling
    $('a').smoothScroll();
});
