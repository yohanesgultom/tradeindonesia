# Trade Indonesia

Indonesian export commodities trading portal

## Development

Prerequisites for development:

1. PHP == 7.1.*
2. Composer >= 1.2.2
3. NPM >= 3.10.10
4. MySQL >= Ver 14.14 distrib 5.7.19

Setup and running steps:

1. Install PHP dependencies `composer install`
1. Instal Node.js dependencies `npm install`
1. Create database in MySQL and put the details in `.env`
1. Migrate database `php artisan migrate:refresh --seed`
1. Compile assets `npm run dev`
1. Run the web app `php artisan serve`

## Production deployment

On shared hosting without ssh access, follow these steps:

1. Dump MySQL database `mysqldump -u<DB_USERNAME> -p <DB_DATABASE> -r tradeindonesia.sql`, clear collation charsets in sql and import it to shared hosting's database
1. Copy `public` to shared hosting's `$HOME/public_html`
1. Copy other directories to shared hosting `$HOME/tradeindonesia`
1. Edit `$HOME/tradeindonesia/.env` to match shared hosting environment
1. Edit `$HOME/public_html/index.php` to replace all `'/../bootstrap/autoload.php'` values with `'/../tradeindonesia/bootstrap/autoload.php'`
