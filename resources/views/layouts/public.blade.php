<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="{{ config('app.locale') }}"> <!--<![endif]-->
<head>
    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ env('APP_NAME') }}</title>
    <meta name="description" content="{{ env('APP_DESC') }}">
    <meta name="keywords" content="{{ env('APP_KEYWORDS') }}">
    <meta name="author" content="{{ env('APP_URL') }}">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="IE=9">
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements. All other JS at the end of file. -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <!--headerIncludes-->
    <!-- CSS
    ================================================== -->
    <link rel="stylesheet" href="{{ asset('flatpack/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/menu.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/flat-ui-slider.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/base.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/skeleton.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/landings.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/landings_layouts.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/box.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/pixicon.css') }}">
    <link rel="stylesheet" href="{{ asset('flatpack/css/animations.min.css') }}">
    <link rel="stylesheet" href="{{ asset('auto-complete/auto-complete.css') }}">

    <!--[if lt IE 9]>
    <script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ asset('flatpack/images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('flatpack/images/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('flatpack/images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ asset('flatpack/images/apple-touch-icon-114x114.png') }}">

    @yield('styles')
</head>
<body>

    <div id="page" class="page">
        <div class="header_nav_1 dark inter_3_bg pix_builder_bg" id="section_intro_3">
            <div class="header_style">
                <div class="container">
                    <div class="sixteen columns firas2">
                        <nav role="navigation" class="navbar navbar-white navbar-embossed navbar-lg pix_nav_1">
                            <div class="containerss">
                                <div class="navbar-header">
                                    <button data-target="#navbar-collapse-02" data-toggle="collapse" class="navbar-toggle" type="button">
                                        <span class="sr-only">Toggle navigation</span>
                                    </button>
                                    <div class="brand"><a href="{{ route('page.index') }}">{{ env('APP_NAME') }}</a></div>
                                </div>
                                <div id="navbar-collapse-02" class="collapse navbar-collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active propClone"><a href="{{ route('page.index') }}">Home</a></li>
                                        <li class="propClone dropdown">
                                            <a class="dropdown-toggle" href="javascript:">Product</a>
                                            <ul class="dropdown-menu">
                                                @foreach ($products as $prod)
                                                <li><a href="{{ route('page.index') }}#{{ $prod->slug }}">{{ $prod->name }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#why-us">Why Us</a></li>
                                        <li class="propClone"><a href="{{ route('page.index') }}#rfq">RFQ</a></li>
                                    </ul>
                                </div><!-- /.navbar-collapse -->
                            </div><!-- /.container -->
                        </nav>
                    </div>
                </div><!-- container -->
            </div>

            @yield('header')

        </div>

        @yield('content')

        <div class="pixfort_party_15" id="section_party_4">
            <div class="foot_st pix_builder_bg" style="outline: none; cursor: default;">
                <div class="container">
                    <div class="sixteen columns">
                        <span class="editContent">
                            <span class="pix_text" style="outline: none; cursor: default;">
                                <span class="rights_st"> All rights reserved Copyright © 2017 {{ env('APP_NAME') }}</span>
                            </span>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- JavaScript
================================================== -->
<script src="{{ asset('flatpack/js/jquery-1.8.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.easing.1.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.common.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('flatpack/js/ticker.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/smoothscroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/appear.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/jquery.ui.touch-punch.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/bootstrap-switch.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/custom1.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/appear.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('flatpack/js/animations.js') }}" type="text/javascript"></script>

<script src="{{ asset('flatpack/js/public.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/jquery.smooth-scroll.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('auto-complete/auto-complete.min.js') }}" type="text/javascript"></script>

@yield('scripts')

</body>
</html>
