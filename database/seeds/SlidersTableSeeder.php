<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class SlidersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dir = public_path('flatpack/images/main');
        $sliders = [
          [
            'title' => 'Trade Indonesia',
            'description' => 'We ship world-wide',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#rfq',
            'filename' => 'image-intro-ship.jpg'
          ],
          [
            'title' => 'Palm Oil',
            'description' => 'world\'s largest palm oil producer',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-palmoil.jpg'
          ],
          [
            'title' => 'Natural Rubber',
            'description' => '2<sup>nd</sup> largest natural rubber producer',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-rubber.jpg'
          ],
          [
            'title' => 'Fish & Seafood',
            'description' => '3<sup>rd</sup> largest producer of wild caught fish',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-seafood.jpg'
          ],
          [
            'title' => 'Cocoa',
            'description' => '3<sup>rd</sup> largest cocoa producer in the world',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-cocoa.jpg'
          ],
          [
            'title' => 'Plywood & Paper',
            'description' => '3<sup>rd</sup> largest plywood producer in the world',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-plywood.jpg'
          ],
          [
            'title' => 'Coffee',
            'description' => '4<sup>th</sup> largest coffee producer in the world',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-coffee.jpg'
          ],
          [
            'title' => 'Coal',
            'description' => '5<sup>th</sup> largest coal producer in the world',
            'btn_text' => 'Submit RFQ',
            'btn_url' => '#',
            'filename' => 'image-intro-coal.jpg'
          ],
        ];

        for ($i = 0; $i < count($sliders); $i++) {
          $s = $sliders[$i];
          $file = new File($dir.DIRECTORY_SEPARATOR.$s['filename']);
          $sliders[$i]['path'] = Storage::disk('public')->putFile(\App\Models\Slider::storageDir, $file);
          $sliders[$i]['created_at'] = new \DateTime();
          if ($sliders[$i]['btn_url'] == '#') {
              $sliders[$i]['btn_url'] = '/product/'.str_slug($sliders[$i]['title']).'/#rfq';
          }
        }

        DB::table('sliders')->insert($sliders);
    }
}
