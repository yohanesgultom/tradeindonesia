@extends('layouts.public')

@section('title', $product->name.' | '.env('APP_NAME'))

@section('scripts')
    <script src="{{ asset('flatpack/js/product.js') }}" type="text/javascript"></script>
    <script src="{{ asset('flatpack/js/rfq.js') }}" type="text/javascript"></script>
@endsection


@section('header')
<div class="big_padding pix_builder_bg" id="section_normal_4_1" data-img="{{ asset('storage/'.$product->image_background) }}">
    <div class="container">
        <div class="sixteen columns center_text">
            <p class="big_title editContent">{{ $product->name }}</p>
            <div>
            <p class="normal_text max_600 light_gray editContent">
                <strong>{!! $product->description !!}</strong>
            </p>
            </div>
        </div>
   </div>
</div>
@endsection

@section('content')
<br>
<div class="pixfort_normal_1">
    <div class="m6_style pix_builder_bg" style="outline: none; cursor: default;">
        <div class="container">
            <p align="justify">
                <img src="/storage/{{ $product->image_profile }}" class="img_style" alt="{{ $product->name }}" align="left">
                {!! $product->content !!}
            </p>
        </div>
    </div>
</div>

@include('rfq', [
    'background_image' => asset('storage/'.$product->image_background),
    'products' => $products,
    'product' => $product
])

@endsection
