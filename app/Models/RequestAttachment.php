<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class RequestAttachment
 * @package App\Models
 * @version August 11, 2017, 11:09 am UTC
 */
class RequestAttachment extends Model
{
    use SoftDeletes;

    public $table = 'request_attachments';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'request_id',
        'path'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'request_id' => 'integer',
        'path' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'request_id' => 'required',
        'path' => 'required',
    ];

    public function request()
    {
        return $this->belongsTo('App\Models\Request');
    }

    /**
     * Set the file attributes
     *
     * @param  UploadedFile $uploadedFile
     * @return void
     */
    public function setFileAttribute($uploadedFile)
    {
        $this->attributes['filename'] = $uploadedFile->getClientOriginalName();
        $this->attributes['mime'] = $uploadedFile->getClientMimeType();
    }
}
