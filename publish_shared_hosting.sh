#!/bin/bash

dbname="tradeindonesia"
dbuser=root
dbpass=root

# clear storage
./delete_storage.sh

# recreate database to reset auto-increment indexes
mysqladmin -u"$dbuser" -p"$dbpass" --force=TRUE drop "$dbname"
mysqladmin -u"$dbuser" -p"$dbpass" --force=TRUE create "$dbname"

# migrate and seed db
php artisan migrate:refresh --seed

# export sql
mysqldump -u"$dbuser" -p"$dbpass" "$dbname" -r database.sql

# zip app and public directories
zip -r app-shared.zip . -x .\* -x \*.zip -x \*.xml -x \*.sql -x \*.sh -x .git/\* -x public/\* -x database/\* -x tests/\* -x webpack.mix.js -x package.json
cd public; zip -r ../public-shared.zip . -x index.php -x .htaccess -x \*.json;cd ..
