<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class WhyUs
 * @package App\Models
 * @version August 22, 2017, 8:22 am UTC
 */
class WhyUs extends Model
{
    use SoftDeletes;

    public $table = 'whyus';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'title',
        'subtitle',
        'icon_class',
        'icon_color',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'title' => 'string',
        'subtitle' => 'string',
        'icon_class' => 'string',
        'icon_color' => 'string',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'title' => 'required',
        'subtitle' => 'required',
    ];


}
