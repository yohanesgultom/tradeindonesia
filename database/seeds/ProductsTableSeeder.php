<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class ProductsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $dirProfile = public_path('flatpack/images/1_normal');
        $dirBackground = public_path('flatpack/images/main');
        $content = "Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        <br><br>
        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
        Lorem ipsum dolor sit amet consectetur adipiscing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua Ut enim ad minim veniam quis nostrud exercitation ullamco laboris nisi ut aliquip ex commodo consequat Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.";

        $products = [
            [
                'name' => 'Palm Oil',
                'description' => 'world\'s largest palm oil producer',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'palm.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-palmoil.jpg',
            ],
            [
                'name' => 'Natural Rubber',
                'description' => '2<sup>nd</sup> largest natural rubber producer',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'rubber.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-rubber.jpg',
            ],
            [
                'name' => 'Fish & Seafood',
                'description' => '3<sup>rd</sup> largest producer of wild caught fish',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'seafood.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-seafood.jpg',
            ],
            [
                'name' => 'Cocoa',
                'description' => '3<sup>rd</sup> largest cocoa producer in the world',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'cocoa.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-cocoa.jpg',
            ],
            [
                'name' => 'Plywood & Paper',
                'description' => '3<sup>rd</sup> largest plywood producer in the world',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'plywood.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-plywood.jpg',
            ],
            [
                'name' => 'Coffee',
                'description' => '4<sup>th</sup> largest coffee producer in the world',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'coffee.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-coffee.jpg',
            ],
            [
                'name' => 'Coal',
                'description' => '5<sup>th</sup> largest coal producer in the world',
                'content' => $content,
                'image_profile' => $dirProfile.DIRECTORY_SEPARATOR.'coal.png',
                'image_background' => $dirBackground.DIRECTORY_SEPARATOR.'image-intro-coal.jpg',
            ],
        ];

        for ($i = 0; $i < count($products); $i++) {
          $p = $products[$i];
          $fileProfile = new File($p['image_profile']);
          $fileBackground = new File($p['image_background']);
          $products[$i]['image_profile'] = Storage::disk('public')->putFile(\App\Models\Product::storageDir['profile'], $fileProfile);
          $products[$i]['image_background'] = Storage::disk('public')->putFile(\App\Models\Product::storageDir['background'], $fileBackground);
          $products[$i]['slug'] = str_slug($products[$i]['name']);
          $products[$i]['created_at'] = new \DateTime();
        }

        DB::table('products')->insert($products);
    }
}
