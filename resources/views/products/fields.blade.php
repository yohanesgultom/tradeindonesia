<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Description Field -->
<div class="form-group col-sm-6">
    {!! Form::label('description', 'Description:') !!}
    {!! Form::text('description', null, ['class' => 'form-control']) !!}
</div>

<!-- Content Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('content', 'Content:') !!}
    {!! Form::textarea('content', null, ['class' => 'form-control']) !!}
</div>

@if (empty($product))
<!-- Image Profile Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_profile', 'Image Profile:') !!}
    {!! Form::file('image_profile_file', ['class' => 'form-control']) !!}
</div>

<!-- Image Background Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image_background', 'Image Background:') !!}
    {!! Form::file('image_background_file', ['class' => 'form-control']) !!}
</div>
@else
<!-- Image Profile Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image_profile', 'Image Profile:') !!}
    <div>
      <img src="/storage/{{ $product->image_profile }}" height="200px" />
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::file('image_profile_file', ['class' => 'form-control']) !!}
</div>

<!-- Image Background Field -->
<div class="form-group col-sm-12 col-lg-12">
    {!! Form::label('image_background', 'Image Background:') !!}
    <div>
      <img src="/storage/{{ $product->image_background }}" height="200px" />
    </div>
</div>

<div class="form-group col-sm-6">
    {!! Form::file('image_background_file', ['class' => 'form-control']) !!}
</div>
@endif

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('products.index') !!}" class="btn btn-default">Cancel</a>
</div>
