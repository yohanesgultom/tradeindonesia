<?php

namespace App\Helpers;

class Custom
{
    public static function html_limit($string, $limit, $end='...')
    {
        return str_limit(strip_tags($string), $limit, $end);
    }
}
