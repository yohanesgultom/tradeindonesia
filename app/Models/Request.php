<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Request
 * @package App\Models
 * @version August 10, 2017, 12:47 pm UTC
 */
class Request extends Model
{
    use SoftDeletes;

    public $table = 'requests';


    protected $dates = ['deleted_at'];


    public $fillable = [
        'fullname',
        'company',
        'position',
        'country',
        'email',
        'phone',
        'keywords',
        'category',
        'qty',
        'unit',
        'details',
        'shipping',
        'unit_price',
        'currency',
        'destination',
        'payment',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'fullname' => 'string',
        'company' => 'string',
        'position' => 'string',
        'country' => 'string',
        'email' => 'string',
        'phone' => 'string',
        'category' => 'string',
        'keywords' => 'string',
        'qty' => 'double',
        'unit' => 'string',
        'details' => 'string',
        'shipping' => 'string',
        'unit_price' => 'double',
        'currency' => 'string',
        'destination' => 'string',
        'payment' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'fullname' => 'required',
        'email' => 'required|email',
        'phone' => ['required', 'regex:/^\+?[0-9]+$/'],
        'keywords' => 'required',
        'category' => 'required',
        'qty' => 'required',
        'unit' => 'required'
    ];

    public const UNITS = [
        '20container' => '20\' Container', 
        '40container' => '40\' Container', 
        '40hqcontainer' => '40\' HQ Container', 
        'bbl' => 'Barrels', 
        'box' => 'Boxes',
        'carton' => 'Cartons',
        'cm3' => 'Cubic Centimeter', 
        'ft3' => 'Cubic Foot', 
        'in3' => 'Cubic Inch', 
        'yd3' => 'Cubic Yard', 
        'dozen' => 'Dozens', 
        'drams' => 'Drams', 
        'gals' => 'Gallons',
        'g' => 'Grams',
        'kg' => 'Kilograms',
        'kl' => 'Kiloliters',
        'l' => 'Liters', 
        'metricton' => 'Metric Tons', 
        'pcs' => 'Pieces',
        'ft2' => 'Square Feet', 
        'm2' => 'Square Meters', 
        'yd2' => 'Square Yard', 
        'tonne' => 'Tonnes', 
        'ton' => 'Tons', 
        'tray' => 'Trays',
    ];

    public const SHIPPING_METHODS = [
        'CIF' => 'CIF',
        'CNF' => 'CNF',
        'EXW' => 'EXW',
        'FOB' => 'FOB',
    ];

    public const CURRENCIES = [
        'AUD' => 'AUD',
        'CNY' => 'CNY',
        'EUR' => 'EUR',
        'GBP' => 'GBP',
        'IDR' => 'IDR',
        'JPY' => 'JPY',
        'SGD' => 'SGD',
        'USD' => 'USD',
    ];

    public const PAYMENT_METHODS = ['T/T' => 'T/T', 'D/P' => 'D/P', 'L/C' => 'L/C'];

    /**
     * Get the attachments for the request.
     */
    public function attachments()
    {
        return $this->hasMany('App\Models\RequestAttachment');
    }
}
