<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Testimonial
 * @package App\Models
 * @version August 19, 2017, 12:16 am UTC
 */
class Testimonial extends Model
{
    use SoftDeletes;

    public $table = 'testimonials';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'source',
        'content',
        'rating'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'source' => 'string',
        'content' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'source' => 'required',
        'content' => 'required'
    ];

    
}
