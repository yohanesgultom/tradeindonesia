<div class="pixfort_gym_13 pix_builder_bg" id="rfq" style="outline: none; cursor: default; background-image: linear-gradient(rgba(0,0,0,0.50),rgba(0,0,0,0.50)), url({{ $background_image }})">
    <div class="join_us_section">
        <h2 class="pix_text center_text">Request For Quotation (RFQ)</h2>

        <div class="pix_form_area">
            <div id="rfq-form" class="substyle pix_builder_bg" style="outline: none; cursor: default;">

                <form action="{{ route('post.rfq-details') }}" class="container" method="post" pix-confirm="hidden_pix_13" enctype="multipart/form-data">
                    <br>

                    <div class="text-style">
                        <span class="editContent">
                            <span class="pix_text" style="outline: none; cursor: default;">
                                The more specific your information, the more accurately we can match your request to the right suppliers
                            </span>
                        </span>
                    </div>

                    <br>

                    <div class="clearfix"></div>

                    <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="six columns">
                            <label>Full Name</label>
                            <input type="text" name="fullname" value="{{ $fullname or '' }}" placeholder="Your Full Name" class="pix_text" required>
                        </div>
                        <div class="six columns">
                            <label>Company Name</label>
                            <input type="text" name="company" value="{{ $company or '' }}" placeholder="Your Company Name" class="pix_text">
                        </div>
                        <div class="four columns">
                            <label>Position</label>
                            <input type="text" name="position" value="{{ $position or '' }}" placeholder="Your Position" class="pix_text">
                        </div>
                    </div>

                    <div class="row">
                        <div class="six columns">
                            <label>Country</label>
                            <input type="text" name="country" value="{{ $country or '' }}" placeholder="Select Your Country" class="pix_text">
                        </div>
                        <div class="six columns">
                            <label>E-mail</label>
                            <input type="email" name="email" value="{{ $email or '' }}" placeholder="Your Email" class="pix_text" required>
                        </div>
                        <div class="four columns">
                            <label>Phone</label>
                            <input type="tel" name="phone" value="{{ $phone or '' }}" placeholder="Your Phone Number" class="pix_text" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="sixteen columns">
                            <label>Keywords</label>
                            <input type="text" name="keywords" placeholder="Keywords of products you are looking for" class="pix_text" required>
                        </div>
                    </div>

                    <div class="row">
                        <div class="ten columns">
                            <label>Category</label>
                            <select name="category">
                            @if (!empty($product))
                                <option value="{{ $product->name }}">{{ $product->name }}</option>
                            @else
                                @foreach ($products as $p)
                                <option value="{{ $p->name }}">{{ $p->name }}</option>
                                @endforeach
                                <option value="Other">Other</option>
                            @endif
                            </select>
                        </div>
                        <div class="three columns">
                            <label>Quantity</label>
                            <input type="number" name="qty" placeholder="Enter quantity" class="pix_text" required>
                        </div>
                        <div class="three columns">
                            <label>Unit</label>
                            {{ Form::select('unit', \App\Models\Request::UNITS, 'ton') }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="sixteen columns">
                            <label>Details</label>
                            <textarea name="details" rows="10" placeholder="Detailed description about your request"></textarea>
                        </div>
                    </div>

                    <div class="row">
                        <div class="sixteen columns">
                            <label>Attachments (Ctrl + click to select multiple files)</label>
                            <input type="file" name="attachmentFiles[]" class="pix_text" multiple>
                        </div>
                    </div>

                    <div id="other-requirements-toggle" class="title-style">
                        <span class="editContent">
                            <span class="pix_text" style="outline: none; cursor: hand;">
                                Other Requirements
                            </span>
                            <span class="fui-triangle-up"></span>
                        </span>
                    </div>
                    <br>
                    <div class="text-style other-requirements">
                        <span class="editContent">
                            <span class="pix_text" style="outline: none; cursor: default;">
                                Describe about your preferred unit price, destination port and payment method
                            </span>
                        </span>
                    </div><br>

                    <div class="row other-requirements">
                        <div class="four columns">
                            <label>Shipping Method</label>
                            {{ Form::select('shipping', \App\Models\Request::SHIPPING_METHODS, 'FOB', ['placeholder' => 'Select shipping method']) }}
                        </div>
                        <div class="four columns">
                            <label>Unit Price</label>
                            <input type="number" name="unit_price" placeholder="Preferred unit price" class="pix_text">
                        </div>
                        <div class="four columns">
                            <label>Currency</label>
                            {{ Form::select('currency', \App\Models\Request::CURRENCIES, 'USD', ['placeholder' => 'Select currency']) }}
                        </div>
                    </div>

                    <div class="row other-requirements">
                        <div class="eight columns">
                            <label>Destination Port</label>
                            <input type="text" name="destination" placeholder="Preferred destination port" class="pix_text">
                        </div>
                        <div class="four columns">
                            <label>Payment Method</label>
                            {{ Form::select('payment', \App\Models\Request::PAYMENT_METHODS, 'L/C', ['placeholder' => 'Select payment']) }}
                        </div>
                    </div>

                    <div class="row">
                        <div class="twelve columns">
                            <span class="send_btn">
                                <button id="submit_btn" class="slow_fade editContent pix_text" style="outline: none">
                                    <span class="editContent">Submit RFQ</span>
                                </button>
                            </span>
                        </div>
                    </div>
                </form>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>

<!-- confirmation -->
<div id="rfq-notification" class="confirm_page confirm_page_13 pix_builder_bg" style="outline: none; cursor: default;">
    <div class="pixfort_gym_13 ">
        <div class="confirm_header">
            <span class="editContent">
              <span class="pix_text" style="outline: none; cursor: default; text-align: center">
                Thank You for Your Request!
              </span>
            </span>
        </div>
        <div class="confirm_text">
            <span class="editContent">
              <span class="pix_text" style="outline: none; cursor: default;">
                Please wait until submission completed..
              </span>
            </span>
        </div>
    </div>
</div>
