<?php

namespace App\Repositories;

use App\Models\Request;
use InfyOm\Generator\Common\BaseRepository;

class RequestRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'fullname',
        'company',
        'email',
        'phone',
        'keywords',
        'category',
        'shipping',
        'currency',
        'destination',
        'payment',
        'status'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Request::class;
    }
}
